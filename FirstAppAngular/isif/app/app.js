// var example= angular.module("example",['ngStorage']);
// example.controller('examplecontroller', function($scope, ))
 angular.module('myApp', ["ngRoute"])
	.config(function($routeProvider) {
	    $routeProvider
	    .when('/', {
	        templateUrl : 'views/login.html',
	        controller: 'loginCtrl'
	    })

	    .when('/user',{
		    	templateUrl: 'views/user.html',
		    	controller: 'userCtrl'
	    })
	    .otherwise({redirectTo : '/'});
	    
	});