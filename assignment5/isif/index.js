var express = require('express');
var path = require('path');


var app = express();


app.use(express.static(path.join(__dirname, '/')));

app.use('/*', function(req, res){
  res.sendFile(path.join(__dirname, '/index.html'));
});
app.use('/*/*', function(req, res){
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.set('port', process.env.PORT || 8080);

var server = app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + server.address().port);
});
