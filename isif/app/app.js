// var example= angular.module("example",['ngStorage']);
// example.controller('examplecontroller', function($scope, ))
 angular.module('myApp', ['ngRoute', 'ngStorage'])
	.config(function($routeProvider) {
	    $routeProvider
	    .when('/', {
	        templateUrl : 'app/views/login.html',
	        controller: 'loginCtrl'
	    })

	    .when('/user',{
		    	templateUrl: 'app/views/user.html',
		    	controller: 'userCtrl'
	    })
	    .when('/insert',{
		    	templateUrl: 'app/views/insert.html',
		    	controller: 'insertCtrl'
	    })
	    .when('/show',{
		    	templateUrl: 'app/views/show.html',
		    	controller: 'showCtrl'
	    })
	    .when('/insert',{
		    	templateUrl: 'app/views/update.html',
		    	controller: 'updateCtrl'
	    })

	    .otherwise({redirectTo : '/'});
	    
	});